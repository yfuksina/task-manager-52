package ru.tsc.fuksina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.service.IConnectionService;
import ru.tsc.fuksina.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.fuksina.tm.dto.model.TaskDTO;
import ru.tsc.fuksina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.fuksina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.fuksina.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.TaskIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;
import ru.tsc.fuksina.tm.repository.dto.ProjectRepositoryDTO;
import ru.tsc.fuksina.tm.repository.dto.TaskRepositoryDTO;

import java.util.List;
import java.util.Optional;

public class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final TaskServiceDTO taskService;

    @NotNull
    private final ProjectServiceDTO projectService;

    public ProjectTaskServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.taskService = new TaskServiceDTO(connectionService);
        this.projectService = new ProjectServiceDTO(connectionService);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskService.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskService.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        tasks.stream()
                .forEach(task -> taskService.removeById(userId, task.getId()));
        projectService.removeById(userId, projectId);
    }

}
