package ru.tsc.fuksina.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.fuksina.tm.api.service.IConnectionService;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.fuksina.tm.dto.model.UserDTO;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.repository.dto.UserRepositoryDTO;
import ru.tsc.fuksina.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

public class UserServiceDTO extends AbstractServiceDTO<UserDTO, IUserRepositoryDTO> implements IUserServiceDTO {

    @NotNull
    private final IPropertyService propertyService;
    
    public UserServiceDTO(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    protected @NotNull UserRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepositoryDTO(entityManager);
    }

    @Nullable
    @Override
    public UserDTO findOneByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final UserRepositoryDTO repository = getRepository(entityManager);
            return repository.findOneByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findOneByLogin(login) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneByEmail(@Nullable final String email) {
        Optional.ofNullable(email).filter(item -> !item.isEmpty()).orElseThrow(EmailEmptyException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final UserRepositoryDTO repository = getRepository(entityManager);
            return repository.findOneByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findOneByEmail(email) != null;
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final UserDTO user = findOneByLogin(login);
        remove(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final UserDTO user = new UserDTO();
        try {
            @NotNull final UserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).filter(item -> !item.isEmpty()).orElseThrow(EmailEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final UserDTO user = new UserDTO();
        try {
            @NotNull final UserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            user.setRole(Role.USUAL);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final UserDTO user = new UserDTO();
        try {
            @NotNull final UserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO setPassword(@Nullable final String userId, @Nullable final String password) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @Nullable final UserDTO user = findOneById(userId);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        update(user);
        return user;
    }

    @Nullable
    @Override
    public UserDTO updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        @Nullable final UserDTO user = findOneById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final UserDTO user= findOneByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final UserDTO user = findOneByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        update(user);
    }

}
