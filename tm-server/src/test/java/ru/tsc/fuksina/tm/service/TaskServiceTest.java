package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.fuksina.tm.api.service.IConnectionService;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.fuksina.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.fuksina.tm.dto.model.TaskDTO;
import ru.tsc.fuksina.tm.dto.model.UserDTO;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.marker.DatabaseCategory;
import ru.tsc.fuksina.tm.service.dto.TaskServiceDTO;
import ru.tsc.fuksina.tm.service.dto.UserServiceDTO;
import ru.tsc.fuksina.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

@Category(DatabaseCategory.class)
public class TaskServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private ITaskServiceDTO taskService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private static String USER_ID_1;

    @NotNull
    private static String USER_ID_2;

    private static long INITIAL_SIZE;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void closeConnection() {
        connectionService.close();
    }

    @Before
    public void init() {
        taskService = new TaskServiceDTO(connectionService);
        userService = new UserServiceDTO(propertyService, connectionService);
        @NotNull final UserDTO userFirst = userService.create("user_1", "user");
        USER_ID_1 = userFirst.getId();
        @NotNull final UserDTO userSecond = userService.create("user_2", "user");
        USER_ID_2 = userSecond.getId();
        taskService.create(USER_ID_1, "task_1", "task_1");
        taskService.create(USER_ID_2, "task_2", "task_2");
        taskService.create(USER_ID_2, "task_3", "task_3");
        INITIAL_SIZE = taskService.getSize();
    }

    @After
    public void end() {
        taskService.clear();
        userService.clear();
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "task"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, ""));
        @NotNull final TaskDTO task = taskService.create(USER_ID_1, "task");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
        Assert.assertNotNull(task.getName());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "task"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, ""));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(USER_ID_1, "task", ""));
        @NotNull final TaskDTO task = taskService.create(USER_ID_1, "task", "description");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
        Assert.assertNotNull(task.getDescription());
    }

    @Test
    public void createWithDescriptionAndDate() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "task"));
        @NotNull final TaskDTO task = taskService.create(
                USER_ID_1,
                "name",
                "description",
                DateUtil.toDate("01.01.2021"),
                DateUtil.toDate("01.10.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
        Assert.assertNotNull(task.getDateStart());
        Assert.assertNotNull(task.getDateEnd());
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(""));
        @NotNull final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertEquals(INITIAL_SIZE, tasks.size());
        @NotNull final List<TaskDTO> userTasks = taskService.findAll(USER_ID_2);
        Assert.assertEquals(2, userTasks.size());
        @NotNull final List<TaskDTO> newUserTasks = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, newUserTasks.size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID_1, ""));
        @NotNull final String taskName = "task find by id";
        @NotNull final TaskDTO task = taskService.create(USER_ID_1, taskName);
        @NotNull final String id = task.getId();
        Assert.assertNotNull(taskService.findOneById(USER_ID_1, id));
        Assert.assertEquals(taskName, taskService.findOneById(USER_ID_1, id).getName());
    }

    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove("", new TaskDTO()));
        @NotNull final TaskDTO task = taskService.create(USER_ID_1, "task remove");
        taskService.remove(USER_ID_1, task);
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_ID_1, ""));
        @NotNull final TaskDTO task = taskService.create(USER_ID_1, "task remove");
        @NotNull final String id = task.getId();
        taskService.removeById(USER_ID_1, id);
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
        Assert.assertNull(taskService.findOneById(id));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(""));
        taskService.clear(USER_ID_1);
        Assert.assertEquals(0, taskService.getSize(USER_ID_1));
        Assert.assertEquals(INITIAL_SIZE - 1, taskService.getSize());
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, ""));
        @NotNull final TaskDTO task = taskService.create(USER_ID_1, "task exists");
        Assert.assertTrue(taskService.existsById(USER_ID_1, task.getId()));
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.updateById("", "123", "new", "new descr")
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> taskService.updateById(USER_ID_1, "", "new", "new descr")
        );
        Assert.assertThrows(NameEmptyException.class,
                () -> taskService.updateById(USER_ID_1, "123", "", "new descr")
        );
        Assert.assertThrows(DescriptionEmptyException.class,
                () -> taskService.updateById(USER_ID_1, "123", "new", "")
        );
        @NotNull final String name = "new name";
        @NotNull final String description = "new description";
        @NotNull final String id = taskService.create(USER_ID_1, "old name").getId();
        @NotNull final TaskDTO task = taskService.updateById(USER_ID_1, id, name, description);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.changeTaskStatusById("", "123", Status.IN_PROGRESS)
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> taskService.changeTaskStatusById(USER_ID_1, "", Status.NOT_STARTED)
        );
        Assert.assertThrows(StatusEmptyException.class,
                () -> taskService.changeTaskStatusById(USER_ID_1, "123", Status.toStatus(""))
        );
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final String id = taskService.create(USER_ID_1, "task change status").getId();
        @NotNull final TaskDTO task = taskService.changeTaskStatusById(USER_ID_1, id, status);
        Assert.assertEquals(status, task.getStatus());
    }

}
