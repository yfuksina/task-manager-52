package ru.tsc.fuksina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
