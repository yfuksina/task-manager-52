package ru.tsc.fuksina.tm.component;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.service.IReceiverService;
import ru.tsc.fuksina.tm.listener.LoggerListener;
import ru.tsc.fuksina.tm.service.ReceiverService;

public class Bootstrap {

    public void run() {
        @NotNull final ActiveMQConnectionFactory factory =
                new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}
